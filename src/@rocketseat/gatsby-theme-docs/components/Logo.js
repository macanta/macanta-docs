import styled from '@emotion/styled';

import logo from '../assets/Macanta-Logo_Colour.jpg';

export default styled.div`
  width: 160px;
  height: 107px;
  background-size: contain;
  background: url(${logo}) center no-repeat;
`;